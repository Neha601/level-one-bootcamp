//WAP to find the sum of two fractions.
#include<stdio.h>
typedef struct fraction
{
int num;
int deno;
}Fraction;
Fraction input()
{
	Fraction f;
	printf("\nEnter the numerator:");
	scanf("%d",&f.num);
	printf("Enter the denominator:");
	scanf("%d",&f.deno);
	return f;
} 
Fraction sum(Fraction f1,Fraction f2)
{
	Fraction r;
	r.num=(f1.num*f2.deno)+(f2.num*f1.deno);
	r.deno=f1.deno*f2.deno;
	return r;
}
void result(Fraction r)
{
	printf("The sum of two fractions is %d/%d",r.num,r.deno);
}
int main()
{
	Fraction f1,f2,res;
	printf("Enter the numerator and denominator of the 1st fraction:");
	f1=input();
	printf("Enter the numerator and denominator of the 2nd fraction:");
	f2=input();
	res=sum(f1,f2);
	result(res);
	return 0;
}